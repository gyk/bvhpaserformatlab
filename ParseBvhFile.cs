﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using Microsoft.Xna.Framework;

namespace BvhPaserForMatlab
{
	public static class ParseBvhFile
	{
		const int NUM_OF_BASE_JOINT = 15;
		public static float[,] ParseFile(string path, int downsamplingRatio)
		{
			float[,] floatArray;
			using (StreamReader bvhReader = new StreamReader(path)) {
				BvhParser parser = new BvhParser(bvhReader);
				var skeleton = new Skeleton();
				var motions = parser.Parse(skeleton);
				Joint[] baseSkeleton = ExtractBaseSkeleton(skeleton);

				int numOfFrame = parser.NumOfFrames / downsamplingRatio;
				floatArray = new float[numOfFrame, (NUM_OF_BASE_JOINT - 1) * 3];
				for (int frame = 0, i = 0; frame < numOfFrame; frame += 1, i += downsamplingRatio) {
					skeleton.ApplyMotion(motions[i]);
					ExtractPositionArray(skeleton, baseSkeleton, floatArray, frame);
				}
			}

			return floatArray;
		}

        //{"Hips", 0},
        //{"LeftUpLeg", 1},
        //{"LeftLeg", 2},
        //{"LeftFoot", 3},
        //{"RightUpLeg", 4},
        //{"RightLeg", 5},
        //{"RightFoot", 6},
        //{"Spine", 7},
        //{"LeftArm", 8},
        //{"LeftForeArm", 9},
        //{"LeftHand", 10},
        //{"RightArm", 11},
        //{"RightForeArm", 12},
        //{"RightHand", 13},
        //{"Head", 14}, 

		static Dictionary<string, int> jointNameToID = new Dictionary<string, int>()
		{
			{"Hip", 0},
			{"Chest", 1},
			{"LShoulder", 2},
			{"LForearm", 3},
			{"LHand", 4},
			{"RShoulder", 5},
			{"RForearm", 6},
			{"RHand", 7},
			{"LThigh", 8},
			{"LShin", 9},
			{"LFoot", 10},
			{"RThigh", 11},
			{"RShin", 12},
			{"RFoot", 13},
			{"Head", 14},
		};

		private static Joint[] ExtractBaseSkeleton(Skeleton skeleton)
		{
			Joint[] baseSkeleton = new Joint[NUM_OF_BASE_JOINT];
			Joint root = skeleton.Root;
			ExtractBaseSkeletonRecursively(root, baseSkeleton);
			return baseSkeleton;
		}

		private static void ExtractBaseSkeletonRecursively(Joint j, Joint[] baseSkeleton)
		{
			int jointID;
			if (jointNameToID.TryGetValue(j.Name, out jointID)) {
				baseSkeleton[jointID] = j;
			}

			foreach (Joint child in j.Children) {
				ExtractBaseSkeletonRecursively(child, baseSkeleton);
			}
		}

		public static void ExtractPositionArray(Skeleton skeleton, Joint[] baseSkeleton, float[,] positionArray, int row)
		{
			Vector3 hipsPos = skeleton.Poses[baseSkeleton[0]].Translation;
			float x = hipsPos.X;
			float y = hipsPos.Y;
			float z = hipsPos.Z;
			for (int i = 1; i < NUM_OF_BASE_JOINT; i++) {
				int i1 = i - 1;
				Vector3 pos = skeleton.Poses[baseSkeleton[i]].Translation;
				positionArray[row, i1 * 3 + 0] = pos.X - x;
				positionArray[row, i1 * 3 + 1] = pos.Y - y;
				positionArray[row, i1 * 3 + 2] = pos.Z - z;
			}
		}
	}
}
